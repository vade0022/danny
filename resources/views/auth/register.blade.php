@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registreer</div>
                <div class="panel-body">
                    <form class="form-horizontal address" role="form" method="POST" name="form1" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Voornaam</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Achternaam</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<br> </br>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Telfoonnummer</label>

                            <div class="col-md-6">
                                <input id="telefoon" type="text" class="form-control" maxlength="10" name="telefoon" value="{{ old('telefoon') }}" required autofocus>

                                @if ($errors->has('telefoon'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Woonplaats') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mailadres</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Wachtwoord</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Bevestig het wachtwoord</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

<br><span class="message"></span><br/> </br>


                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="postcode" class="col-md-4 control-label">Postcode</label>

                            <div class="col-md-6">
                                <input id="postcode" type="text" class="form-control postcode" name="postcode" maxlength="6" value="{{ old('postcode') }}" required autofocus >

                                @if ($errors->has('postcode'))
                                    <span class="help-block ">
                                        <strong>{{ $errors->first('Postcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Huisnummer + Toevoegingen</label>

                            <div class="col-md-6">
                                <input id="streetnumber" type="text" class="form-control streetnumber" name="streetnumber" value="{{ old('streetnumber') }}" required autofocus>

                                @if ($errors->has('streetnumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('streetnumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Straatnaam</label>

                            <div class="col-md-6">
                                <input id="straatnaam" type="text" class="form-control street" readonly name="straatnaam" value="{{ old('straatnaam') }}" required autofocus>

                                @if ($errors->has('straatnaam'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('straatnaam') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Woonplaats</label>

                            <div class="col-md-6">
                                <input id="woonplaats" type="text" class="form-control city" readonly name="woonplaats" value="{{ old('woonplaats') }}" required autofocus>

                                @if ($errors->has('woonplaats'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('woonplaats') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="provincie" class="col-md-4 control-label">Provincie</label>

                            <div class="col-md-6">
                                <input id="provincie" type="text" class="form-control province" readonly name="provincie" value="{{ old('provincie') }}" required autofocus>

                                @if ($errors->has('Provincie'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Provincie') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registreer
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
  <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="js/autocomplete.js"></script>
<script>
  var pro6pp_auth_key = "PF8KTJWAzx8OzSut";
  $(document).ready(function() {
    $(".address").applyAutocomplete();
  });
  </script>

@endsection
